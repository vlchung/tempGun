Temp Gun
==========
An Arduino Micro-based project, this project is configured in the following
manner:

LCD (HD44780-compatible) 20x4 display connected via the following DIO pins:
  RS: 2
  Enable: 3
  D4: 4
  D5: 5
  D6: 6
  D7: 7

And a MLX90614 connected to the I2C bus.
