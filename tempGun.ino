#include <Wire.h> // Include Wire.h - Arduino I2C library
#include <Adafruit_MLX90614.h>
#include "ScrollyLcd.h"

ScrollyLcd   lcd(2,3,4,5,6,7);

Adafruit_MLX90614 tempSensor = Adafruit_MLX90614();

void setup() {
  // put your setup code here, to run once:
  Serial.begin(38400);
  Serial.println("Starting");
  lcd.begin(20, 4);
  tempSensor.begin();  
}

long theTime = 0;
void loop() {
  // put your main code here, to run repeatedly:

  double temp, ambient;

  if (millis() - theTime > 1000) {
    temp = tempSensor.readObjectTempC();
    ambient = tempSensor.readAmbientTempC();
    Serial.println("T: " + String(temp, 1) + " A: " + String(ambient, 1));
    lcd.println();
    lcd.print("T: " + String(temp, 1) + " A: " + String(ambient, 1));
    theTime = millis();
  }
}
